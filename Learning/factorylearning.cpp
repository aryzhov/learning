#include "factorylearning.h"
#include "LearningCast.h"
#include "learning.h"
#include "learningoperator.h"

Learning* FactoryLearning::Create(FactoryLearning::Type type)
{
    Learning* obj = nullptr;
    switch (type) {
    case Type::Cast:
        obj = new LearningCast;
        break;

    case Type::Operator:
        obj = new LearningOperator;
        break;
    }

    return obj;
}
