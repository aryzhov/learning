#pragma once
class Base
{
public:
	Base();
	virtual ~Base();

	virtual void foo() const;

	void methodBase();

private:
	int i = 1;
};

