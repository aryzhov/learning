#pragma once
#include "SubBase.h"
#include "SecondSubBase.h"

class MultipleSubBase :
	public SubBase,  public SecondSubBase
{
public:
	MultipleSubBase();
	virtual ~MultipleSubBase();

	virtual void foo()  const override;

	void methodMultipleSubBase();

private:
	int i = 4;
};

