#include "Base.h"
#include <iostream>


Base::Base()
{

	std::cout << "Constructor Base class" << std::endl;
}


Base::~Base()
{
	std::cout << "Destructor Base class" << std::endl;
}

void Base::foo()  const
{
	std::cout << "Foo Base class with value: " << i << std::endl;
}

void Base::methodBase()
{
	foo();
}
