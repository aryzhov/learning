#pragma once
#include "Base.h"
class SubBase :
	public Base
{
public:
	SubBase();
	virtual ~SubBase();

	virtual void foo()  const override;

	void methodSubBase() const;

private:
	int i = 2;
};

