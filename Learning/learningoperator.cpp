#include "learningoperator.h"
#include <iostream>
#include <string>

class ImplicitConversion {
public:
    ImplicitConversion(int value)
        : i(value)
    {
    }

    operator double() const
    {
        return i;
    }

private:
    int i = 0;
};

void printImpl(double)
{
    std::cout << "Call functin with double" << std::endl;
}

void printImpl(int)
{
    std::cout << "Call functin with integer" << std::endl;
}

void foo() {}

class ClassWithStringConstructor {
public:
    /*explicit*/ ClassWithStringConstructor(const std::string& s)
        : something(s)
    {
    }

    void print() const
    {
        std::cout << something << std::endl;
        ++counter;
    }

    void printCounter() const
    {
        std::cout << counter << std::endl;
    }

private:
    std::string something;
    mutable int counter = 0;
};

void implicitConversionFunction(const ClassWithStringConstructor& obj)
{
    obj.print();
    obj.printCounter();
}

class ClassWithExplicitStringConstrucotr {
public:
    explicit ClassWithExplicitStringConstrucotr(const std::string& s)
        : something(s)
    {
    }

    void print() const
    {
        std::cout << something << std::endl;
        ++counter;
    }

    void printCounter() const
    {
        std::cout << counter << std::endl;
    }

private:
    std::string something;
    mutable int counter = 0;
};

void explicitConversionFunction(const ClassWithExplicitStringConstrucotr& obj)
{
    obj.print();
    obj.printCounter();
}

class IncrementDecrementOperator {
public:
    explicit IncrementDecrementOperator(int i)
        : index(i)
    {
    }
    // prefix
    IncrementDecrementOperator& operator++()
    {
        std::cout << "Prefix increment" << std::endl;
        ++index;
        return *this;
    }
    IncrementDecrementOperator& operator--()
    {
        std::cout << "Prefix decrement" << std::endl;
        --index;
        return *this;
    }

    // postfix
    IncrementDecrementOperator operator++(int)
    {
        std::cout << "Postfix increment" << std::endl;

        IncrementDecrementOperator old = *this;

        ++*this;
        return old;
    }
    IncrementDecrementOperator operator--(int)
    {
        std::cout << "Postfix decrement" << std::endl;
        IncrementDecrementOperator old = *this;

        --*this;
        return old;
    }

    int getIndex() const
    {
        return index;
    }

private:
    int index;
};

void LearningOperator::run()
{
    ImplicitConversion imp(4);

    ImplicitConversion impl(); // function declared but not an object of class ImplicitConversion

    std::cout << imp << std::endl; // out: 4
    std::cout << impl << std::endl; // out 1 - conversion point impl  to bool
    std::cout << foo << std::endl; // out 1 - conversion point foo to bool

    int i = 1;
    auto sum = imp + i;

    printImpl(sum);

    const std::string str = "string value";

    // implicit constructor
    implicitConversionFunction(str);
    // implicitConversionFunction("we use two implicit conversions"); // compile error
    implicitConversionFunction(std::string("we use one implicit conversions and one explicit")); // compile ok

    // explicit constructor
    // explicitConversionFunction(str); // compile error
    // explicitConversionFunction(std::string("we use one implicit conversions and one explicit")); // compile error
    explicitConversionFunction(ClassWithExplicitStringConstrucotr(str)); // compile ok

    IncrementDecrementOperator incr_decr(10);

    ++incr_decr; // 11
    incr_decr++ ++++++++++++++++++++++++++++; // 12
    ++++++incr_decr; // 15

    std::cout << incr_decr.getIndex() << std::endl; // output: 15

    int incr_decr_int = 10;
    ++incr_decr_int; // 11
    // incr_decr_int++ ++++++++++++++++++++++++++++; // compile error
    ++++incr_decr_int; // 13

    std::cout << incr_decr_int << std::endl; // output: 13
}
