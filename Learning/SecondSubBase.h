#pragma once
#include "Base.h"
class SecondSubBase :
	public Base
{
public:
	SecondSubBase();
	virtual ~SecondSubBase();

	virtual void foo()  const override;

	void methodSecondSubBase();

private: 
	int i = 3;
};

