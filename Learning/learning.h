#pragma once

class Learning {
public:
    virtual ~Learning();

    virtual void run() = 0;
};
