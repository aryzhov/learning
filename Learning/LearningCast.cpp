#include "LearningCast.h"
#include "MultipleSubBase.h"
#include <iostream>

void foo(Base* base);
void fooMultiple(MultipleSubBase* multiple);

void LearningCast::run()
{
    auto base = new Base;

    foo(base);

    delete base;
    /* Output:
	Bad cast Base to SubBase
	Bad cast Base to SecondSubBase
	Bad cast Base to MultipleSubBase
	*/

    auto subBase = new SubBase;

    foo(subBase);

    delete subBase;
    /* Output:
	Success cast Base to SubBase
	Foo SubBase class with value: 2
	Bad cast Base to SecondSubBase
	Bad cast Base to MultipleSubBase
	*/

    auto secondSubBase = new SecondSubBase;

    foo(secondSubBase);

    delete secondSubBase;
    /* Output:
	Bad cast Base to SubBase
	Success cast Base to SecondSubBase
	Foo SecondSubBase class with value: 3
	Bad cast Base to MultipleSubBase
	*/

    auto multiple = new MultipleSubBase;

    fooMultiple(multiple);

    // foo(multiple); compile error

    delete multiple;

    /* Output:
	Success cast MultipleSubBase to SecondSubBase
	Foo SecondSubBase class with value: 4
	Success cast MultipleSubBase to SubBase
	Foo SecondSubBase class with value: 4
	*/
}

void foo(Base* base)
{
    auto subBase = dynamic_cast<SubBase*>(base);

    if (subBase) {
        std::cout << "Success cast Base to SubBase" << std::endl;
        subBase->methodSubBase();

        if (auto second = dynamic_cast<SecondSubBase*>(subBase)) {
            std::cout << "Success cast SubBase to SecondSubBase" << std::endl;
            second->methodSecondSubBase();
        }
    } else {
        std::cout << "Bad cast Base to SubBase" << std::endl;
    }

    auto secondSubBase = dynamic_cast<SecondSubBase*>(base);

    if (secondSubBase) {
        std::cout << "Success cast Base to SecondSubBase" << std::endl;
        secondSubBase->methodSecondSubBase();

        if (auto sub = dynamic_cast<SubBase*>(secondSubBase)) {
            std::cout << "Success cast SecondSubBase to SubBase" << std::endl;
            sub->methodSubBase();
        }
    } else {
        std::cout << "Bad cast Base to SecondSubBase" << std::endl;
    }

    auto multiple = dynamic_cast<MultipleSubBase*>(base);

    if (multiple) {
        std::cout << "Success cast from Base to MultipleSubBase" << std::endl;
        multiple->methodMultipleSubBase();
    } else {
        std::cout << "Bad cast Base to MultipleSubBase" << std::endl;
    }
}

void fooMultiple(MultipleSubBase* multiple)
{
    auto secondSubBase = dynamic_cast<SecondSubBase*>(multiple);
    if (secondSubBase) {
        std::cout << "Success cast MultipleSubBase to SecondSubBase" << std::endl;
        secondSubBase->methodSecondSubBase();
    } else {
        std::cout << "Bad cast MultipleSubBase to SecondSubBase" << std::endl;
    }

    auto subBase = dynamic_cast<SubBase*>(multiple);
    if (subBase) {
        std::cout << "Success cast MultipleSubBase to SubBase" << std::endl;
        subBase->methodSubBase();
    } else {
        std::cout << "Bad cast MultipleSubBase to SubBase" << std::endl;
    }

    // auto base = dynamic_cast<Base *>(multiple); // compile error
}
