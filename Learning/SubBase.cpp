#include "SubBase.h"
#include <iostream>

SubBase::SubBase()
{
	std::cout << "Constructor SubBase class" << std::endl;
}


SubBase::~SubBase()
{
	std::cout << "Destructor SubBase class" << std::endl;
}

void SubBase::foo()  const
{
	std::cout << "Foo SubBase class with value: " << i << std::endl;
}

void SubBase::methodSubBase() const
{
	foo();
}
