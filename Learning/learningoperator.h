#pragma once
#include "learning.h"

class LearningOperator : public Learning {
public:
    virtual void run() override;
};
