#pragma once
#include "learning.h"

class FactoryLearning {
public:
    enum class Type {
        Cast,
        Operator
    };

    static Learning* Create(Type);
};
