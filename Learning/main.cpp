#include "factorylearning.h"
#include <iostream>
#include <string>

FactoryLearning::Type GetType(const std::string& typeInStr);

int main(int argc, char** argv)
{
    if (argc <= 1) {
        std::cout << "Type learning not set" << std::endl;
        return 0;
    }

    const auto type = GetType(argv[1]);

    auto learning = FactoryLearning::Create(type);

    if (learning) {
        learning->run();

        delete learning;
    }

    return 0;
}

FactoryLearning::Type GetType(const std::string& typeInStr)
{
    if (typeInStr == "cast") {
        return FactoryLearning::Type::Cast;
    } else if (typeInStr == "operator") {
        return FactoryLearning::Type::Operator;
    }

    std::cerr << "The type is not found in the arguments or is specified incorrectly. The type must be lowercase." << std::endl;

    return FactoryLearning::Type::Cast;
}
