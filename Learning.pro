# Created by and for Qt Creator This file was created for editing the project sources only.
# You may attempt to use it for building too, by modifying this file here.

#TARGET = Learning

HEADERS = \
   $$PWD/Learning/Base.h \
   $$PWD/Learning/LearningCast.h \
   $$PWD/Learning/MultipleSubBase.h \
   $$PWD/Learning/SecondSubBase.h \
   $$PWD/Learning/SubBase.h \
    $$PWD/Learning/learningoperator.h \
    $$PWD/Learning/factorylearning.h \
    $$PWD/Learning/learning.h

SOURCES = \
   $$PWD/Learning/Base.cpp \
   $$PWD/Learning/LearningCast.cpp \
   $$PWD/Learning/main.cpp \
   $$PWD/Learning/MultipleSubBase.cpp \
   $$PWD/Learning/SecondSubBase.cpp \
   $$PWD/Learning/SubBase.cpp \
    $$PWD/Learning/learningoperator.cpp \
    $$PWD/Learning/factorylearning.cpp \
    $$PWD/Learning/learning.cpp

INCLUDEPATH = \
    $$PWD/Learning

#DEFINES = 

